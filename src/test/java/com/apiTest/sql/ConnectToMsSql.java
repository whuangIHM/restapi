package com.apiTest.sql;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class ConnectToMsSql {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		
/*   		Here are the steps if you want to do this from Eclipse :
			1) Create a folder 'sqlauth' in your C: drive, and copy the dll file sqljdbc_auth.dll to the folder
			1) Go to Run> Run Configurations
			2) Choose the 'Arguments' tab for your class
			3) Add the below code in VM arguments:
			-Djava.library.path=".\sqlauth"
			4) Hit 'Apply' and click 'Run'
*/	
		//Loading the required JDBC Driver class
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");	
		
		//Creating a connection to the database and Create a variable for the connection string, username and password.
        String connectionUrl = "jdbc:sqlserver://STFNYPRODSQL1:1433;databaseName=LoadTest2010;integratedSecurity=true";
        String username = "FLOWERS_NT\\loadtest";
        String password = "NO PASSWORD";
		Connection conn = DriverManager.getConnection(connectionUrl,username,password);

		//Executing SQL query and fetching the result
		Statement st = conn.createStatement();
		String sqlStr = "select count(*) as total from LoadTestRun";
		ResultSet rs = st.executeQuery(sqlStr);
		while (rs.next()) {
			System.out.println("Total Count: "+rs.getString("total"));
		}			
	}
}
