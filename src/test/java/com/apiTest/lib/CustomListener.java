package com.apiTest.lib;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class CustomListener extends TestListenerAdapter {
   private int m_count = 0;
	 
   @Override
   public void onTestFailure(ITestResult tr) {
      log(tr.getName()+ "--TEST METHOD FAILED\n");
   }
	 
   @Override
   public void onTestSkipped(ITestResult tr) {
      log(tr.getName()+ "--TEST METHOD SKIPPED\n");
   }
	 
   @Override
   public void onTestSuccess(ITestResult tr) {
      log(tr.getName()+ "--TEST METHOD SUCCESS\n");
   }
	 
   private void log(String string) {
      System.out.print(string);
      if (++m_count % 40 == 0) {
         System.out.println("");
      }
   }
//implements IMethodInterceptor  {
//	@Override
//	public List<IMethodInstance> intercept(List<IMethodInstance> methods, ITestContext context) {
//		List<IMethodInstance> result = new ArrayList<IMethodInstance>();
//		for (IMethodInstance method : methods) {
//			Test testMethod = method.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class);
//			if (testMethod.priority() == 1) {
//				result.add(method);
//			}
//		}
//		return result;
//	}
	
//    //You have to watch out to get the right test if you have other tests in your suite        
//    if (!paramITestContext.getName().equals("UnwantedTest")) {
//        for (IMethodInstance iMethodInstance : paramList) {
//            Object[] obj = iMethodInstance.getInstances();
//            if (obj[0] instanceof Class1) {
//                //DO your stuff like putting it in a list/array
//            } else {
//                //DO your stuff like putting it in a list/array with the other Testclasses
//            }
//        }
//    }
//    List<IMethodInstance> result = new ArrayList<IMethodInstance>();
//            //Put the results in the results
//    }
//	@Override
//	public List<IMethodInstance> intercept(List<IMethodInstance> methods, ITestContext context) {
//	List<IMethodInstance> result = new ArrayList<IMethodInstance>();
//	for (IMethodInstance method : methods) {
//		Test testMethod = method.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class);
//		Set<String> groups = new HashSet<String>();
//		 for (String group : testMethod.groups()) {
//			 groups.add(group);
//		}
//		 for(String dependongroup : testMethod.dependsOnGroups()) {
//			 groups.add(dependongroup);
//		 }
//		 for(String dependsonmethod : testMethod.dependsOnMethods()) {
//			 groups.add(dependsonmethod);
//		 }
//		 
//		if (groups.contains("testpostCart")) {
//			  result.add(method);
//	    }
//	}
//	
//	return result;
//}
}