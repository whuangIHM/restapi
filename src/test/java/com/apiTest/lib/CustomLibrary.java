package com.apiTest.lib;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.fluttercode.datafactory.impl.DataFactory;
import org.testng.ITestContext;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public interface CustomLibrary {
	DataFactory df = new DataFactory();
	Random rand = new Random();
	SoftAssert softAssert = new SoftAssert();

	@Test
	public static void randomPhone(ITestContext context) {

		int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
		int num2 = rand.nextInt(743);
		int num3 = rand.nextInt(10000);

		DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
		DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros

		String phoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);

		System.out.println("Random Phone: " + phoneNumber);
		context.setAttribute("phone", phoneNumber);
	}

	@Test
	public static void randomName(ITestContext context) {
		String firstName = df.getFirstName();
		String lastName = df.getLastName();
		System.out.println("Random F_Name: " + firstName + " L_Name: " + lastName);
		context.setAttribute("firstName", firstName);
		context.setAttribute("lastName", lastName);
	}

	@Test
	public static void randomEmail(ITestContext context) {
		String email = df.getEmailAddress();
		System.out.println("Random Email: " + email);
		context.setAttribute("email", email);
	}

	@Test
	public static void randomAddress(ITestContext context) {
		String address1 = df.getAddress();
		System.out.println("Random Address1: " + address1);
		context.setAttribute("address1", address1);
	}

	@Test
	public static void getDeliveryDate(ITestContext context) {
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		Date minDate = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 7);
		Date maxDate = c.getTime();
		Date date = df.getDateBetween(minDate, maxDate);
		System.out.println("minDate = " + dt.format(minDate));
		System.out.println("maxDate = " + dt.format(maxDate));
		System.out.println("selDate = " + dt.format(date));
		String startDate = dt.format(date);
		context.setAttribute("startDate", startDate);
	}
}
