package com.apiTest.csv;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;

public class CSVDataProvider {
	private static CSVReader reader = null;
	private static Object[][] data = null;

	// 1_Get Path File location
	private static String getPath(String fileName) {
		ClassLoader classLoader = new CSVDataProvider().getClass().getClassLoader();
		String path = classLoader.getResource(fileName).getFile();
		System.out.println("Path: " + path);
		return path;
	}

	// 2_Get Data From CSV
	private static void getData(String fileName, int totalRow, int totalColumn) {
		int i = 0;
		try {

			// Object[Row][Column]
			data = new Object[totalRow - 1][totalColumn];
			reader = new CSVReaderBuilder(new FileReader(getPath(fileName))).withSkipLines(1).build();
			// read line by line
			String[] record = null;
			while ((record = reader.readNext()) != null && i != (totalRow - 1)) {
				for (int j = 0; j < totalColumn; j++) {
					data[i][j] = record[j];
				}
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// COMPELTE Set Data to CSV
	@SuppressWarnings("resource")
	public static void setData(String headers,String fileName, String testCase, String orderId, String response, String testStatus) {
		// 1 get Path
		String path = CSVDataProvider.class.getClassLoader().getResource("com/apiTest/csv").getPath();
		path = path + "/" + fileName;
		System.out.println("Results Path:"+path);
		File file = new File(path);
		boolean alreadyExists = new File(path).exists();
		try {
			// create FileWriter object with file as parameter
			FileWriter outputfile = new FileWriter(file, true);
			// create CSVWriter object FileWriter object as parameter
			//CSVWriter writer = new CSVWriter(outputfile, CSVWriter.DEFAULT_SEPARATOR, '\t',CSVWriter.NO_QUOTE_CHARACTER);
			@SuppressWarnings("deprecation")
			CSVWriter writer = new CSVWriter(outputfile,CSVWriter.DEFAULT_SEPARATOR);
			@SuppressWarnings("deprecation")
			CSVWriter writerHeader = new CSVWriter(outputfile,CSVWriter.DEFAULT_SEPARATOR, '\t', CSVWriter.NO_QUOTE_CHARACTER);
			// add header to CSV
			if (!alreadyExists) {
				String[] header = { headers };
				writerHeader.writeNext(header, false);
			}
			// add data to CSV
			String[] data = { testCase, orderId, response, testStatus };
			writer.writeNext(data, false);
			// closing writer connection
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 3_Assign Value to Data Provider
	public static Object[][] getCSVData(String fileName, int totalRow, int totalColumn) {
		getData(fileName, totalRow, totalColumn);
		return data;
	}
}
