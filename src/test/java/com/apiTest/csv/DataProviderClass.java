package com.apiTest.csv;

import org.testng.annotations.DataProvider;

public class DataProviderClass {
	@DataProvider(name = "postCartData")
	public static Object[][] postCartData() {
		// Define CSV file name, total row, total column setup
		return CSVDataProvider.getCSVData("PostCart.csv", 2, 9);
	}

	@DataProvider(name = "postCartData_1")
	public static Object[][] postCartData_1() {
		// Define CSV file name, total row, total column setup
		return CSVDataProvider.getCSVData("PostCart2.csv", 3, 9);
	}
	// Debug Use Only
	@DataProvider(name = "debug")
	public static Object[][] debug() {
		// Debug for Get Cart Order ID......
		return new Object[][] { { "123", "123", "123", "flowers" } };
	}
}
