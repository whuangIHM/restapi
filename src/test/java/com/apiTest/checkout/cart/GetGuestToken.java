package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;
import com.google.gson.JsonObject;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class GetGuestToken {
	// Example: https://uat1.origin-gcp.800-flowers.net/r/api/session/guesttoken
	@Test
	public static void getGuestToken(ITestContext context) {
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		JsonObject postBody = new JsonObject();
		postBody.addProperty("guid", (String) context.getAttribute("guid"));
		httpRequest.body(postBody.toString());
		Response response = httpRequest.request(Method.POST, "/r/api/session/guesttoken");
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String accessToken = response.jsonPath().getString("accessToken");
		context.setAttribute("accessToken", accessToken);
		CustomLibrary.softAssert.assertAll();
	}
}
