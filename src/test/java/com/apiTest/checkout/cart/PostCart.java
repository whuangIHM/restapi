package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.csv.CSVDataProvider;
import com.apiTest.lib.CustomLibrary;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class PostCart{
	// Example: https://uat1.origin-gcp.800-flowers.net/checkout/cart?brand=flowers
	static String headers = "TestCase,OrderId,Response,Status,Addtional";
	@Test
	public static void postCart(ITestContext context) {

		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String brandCode = (String) context.getAttribute("brandCode");
		String productCode = (String) context.getAttribute("productCode");
		String quantity = (String) context.getAttribute("quantity");
		String postBody = "{\"product\": { "
						+ "\"brandCode\": \""+brandCode+"\","
						+ "\"categoryId\": \"\","
						+ "\"productCode\": \""+productCode+"\","
						+ "\"productId\": \"\","
						+ "\"quantity\": \""+quantity+"\" } }";
		System.out.println("=======================================================================");
		System.out.println("PostCart===============================================================");
		System.out.println("=======================================================================");
		System.out.println("Test Data(" + testCase + "," + brand + "," + postBody + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(postBody);
		Response response = httpRequest.request(Method.POST, "/r/api/checkout/cart?brand=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		String orderId = response.jsonPath().get("orderId[0]");
		String orderItemId = response.jsonPath().get("orderItemId").toString();

		if (orderId.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Order ID Found******************************");
		CSVDataProvider.setData(headers,"testPostCart_Func.csv", testCase, "No Order ID Found", "*******************","Fail");
		} else {
			context.setAttribute("response", responseBody);
			context.setAttribute("orderId", orderId);
			context.setAttribute("orderItemId", orderItemId);
//			String skuCatentryId = response.jsonPath().get("skuCatentryId").toString();
//			String redirecturl = response.jsonPath().get("redirecturl").toString();
//			String miniShopCartDisplayInd = response.jsonPath().get("miniShopCartDisplayInd").toString();
//			String storeId = response.jsonPath().get("storeId").toString();
//			CSVDataProvider.setData("testPostCart_Func.csv", testCase, orderId, redirecturl+","+orderItemId+","+skuCatentryId+","+miniShopCartDisplayInd+","+storeId,"Pass");
			System.out.println("OrderId: " + orderId + " **** OrderItemId:" + orderItemId);
			System.out.println("Response: " + responseBody);
			CSVDataProvider.setData(headers,"testPostCart_Func.csv", testCase, orderId, responseBody.toString(),"Pass");

		}
		CustomLibrary.softAssert.assertAll();
	}
}
