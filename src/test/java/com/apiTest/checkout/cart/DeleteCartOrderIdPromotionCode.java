package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class DeleteCartOrderIdPromotionCode {
	@Test
	public static void deleteCartOrderIdPromotionCode(ITestContext context) {
		String testCase = (String) context.getAttribute("testCase");
		String orderId = (String) context.getAttribute("orderId");
		String promotionCode = (String) context.getAttribute("promotionCode");
		String brand = (String) context.getAttribute("brand");
		System.out.println("=======================================================================");
		System.out.println("DeleteCartOrderIdPromotionCode=========================================");
		System.out.println("=======================================================================");
		System.out.println("Test Data(" + testCase + "," + orderId + "," + promotionCode + "," + brand + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		Response response = httpRequest.request(Method.DELETE,
				"/r/api/checkout/cart/" + orderId + "/promotion/" + promotionCode + "?brand=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderId: " + orderId);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
