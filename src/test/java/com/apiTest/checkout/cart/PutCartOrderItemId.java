package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class PutCartOrderItemId {
	// https://uat1.origin-gcp.800-flowers.net/checkout/cart/123?brand=flowers
	@Test
	public static void putCartOrderItemId(ITestContext context) {
		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String orderItemId = (String) context.getAttribute("orderItemId");
		String putBody = "{ \"delivery\": { \"backupProductCode\": \"string\","
						+ "\"calendarDateType\": \"string\","
						+ "\"calendarStartDate\": \"string\","
						+ "\"deliveryDate\": \"string\", "
						+ "\"deliverySLA\": \"string\","
						+ "\"deliveryTimeWindow\": \"string\","
						+ "\"flex\": { \"flexMessages\": [ { \"flxMsg\": \"string\", \"flxMsgLoc\": \"string\" } ],"
						+ "\"flexOptionId\": \"string\", \"flexValue\": \"string\", \"forcedDate\": \"string\" },"
						+ "\"holidayCode\": \"string\","
						+ "\"locationType\": \"string\","
						+ "\"useDateForAll\": \"string\","
						+ "\"zipCode\": \"string\" },"
						+ "\"greeting\": { \"message\": \"string\","
						+ "\"occasionCode\": \"string\","
						+ "\"useThisMessageForAll\": \"string\" },"
						+ "\"personalization\": { \"isSkuPersonalized\": \"string\","
						+ "\"personalizableFlag\": \"string\", "
						+ "\"personalizationLines\": [ { \"perAttributeId\": \"string\", \"perAttributeValue\": \"string\" } ],"
						+ "\"personalizationType\": \"string\","
						+ "\"productPersonalization\":"
						+ "{ \"action\": \"string\","
						+ "\"brandCode\": \"string\","
						+ "\"key_id\": \"string\", "
						+ "\"personalizations_id\": \"string\","
						+ "\"productCode\": \"string\", "
						+ "\"text\": \"string\", "
						+ "\"thumbnail\": \"string\" } },"
						+ "\"quantity\": \"string\" }";
		System.out.println("=======================================================================");
		System.out.println("PutCartOrderItemId=====================================================");
		System.out.println("=======================================================================");
		System.out.println("Test Data(" + testCase + "," + orderItemId + "," + brand + "," + putBody + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(putBody);
		Response response = httpRequest.request(Method.PUT, "/r/api/checkout/cart/" + orderItemId + "?brand=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderItem_Id:" + orderItemId);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
