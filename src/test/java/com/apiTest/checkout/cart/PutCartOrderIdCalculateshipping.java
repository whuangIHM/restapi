package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class PutCartOrderIdCalculateshipping {
	// https://uat1.origin-gcp.800-flowers.net/checkout/cart/123/calculateShipping?brand=flowers
	@Test
	public static void putCartOrderIdCalculateshipping(ITestContext context) {
		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String orderId = (String) context.getAttribute("orderId");
		String estimateShipping = (String) context.getAttribute("estimateShipping");
		String putBody = "{\"estimateShipping\":" +estimateShipping+ "}";
		System.out.println("=======================================================================");
		System.out.println("PutCartOrderIdCalculateshipping========================================");
		System.out.println("=======================================================================");
		System.out.println("Test Data(" + testCase + "," + orderId + "," + brand + "," + putBody + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(putBody);
		Response response = httpRequest.request(Method.PUT,
				"/r/api/checkout/cart/" + orderId + "/calculateShipping?brand=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderId: " + orderId);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
