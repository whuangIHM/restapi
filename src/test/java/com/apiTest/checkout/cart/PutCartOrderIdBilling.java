package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class PutCartOrderIdBilling {
	// https://uat1.origin-gcp.800-flowers.net/checkout/cart/123/billing?brand=flowers
	@Test
	public static void putCartOrderIdBilling(ITestContext context) {
		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String orderId = (String) context.getAttribute("orderId");
		String putBody = "{" + "\"billingAddress\": {" + "\"address1\": \"1 old coutry rd\"," 
				+ "\"address2\": \"\","
				+ "\"address3\": \"\"," 
				+ "\"addressId\": \"\"," 
				+ "\"city\": \"Carle Place\",\n"
				+ "\"confirmEmail\": \"test@test.com\"," 
				+ "\"country\": \"USA\"," 
				+ "\"email\": \"test@test.com\","
				+ "\"firstName\": \"Wei\"," 
				+ "\"lastName\": \"Huang\"," 
				+ "\"locationType\": \"Residence\","
				+ "\"organizationName\": \"QA TEST COMP\"," 
				+ "\"phone\": \"1234567890\","
				+ "\"state\": \"NY\","
				+ "\"zipCode\": \"11514\"" + "}" + "}";
		System.out.println("=======================================================================");
		System.out.println("PutCartOrderIdBilling==================================================");
		System.out.println("=======================================================================");
		System.out.println("Test Data(" + testCase + "," + orderId + "," + brand + "," + putBody + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(putBody);
		Response response = httpRequest.request(Method.PUT,
				"/r/api/checkout/cart/" + orderId + "/billing?brand=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderId: " + orderId);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
