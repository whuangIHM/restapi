package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */

public class PutCartOrderItemIdRecipient {
	// https://uat1.origin-gcp.800-flowers.net/checkout/cart/123/recipient?brand=flowers
	@Test
	public static void putCartOrderItemIdRecipient(ITestContext context) {
		System.out.println("=======================================================================");
		System.out.println("PutCartOrderItemIdRecipient============================================");
		System.out.println("=======================================================================");
		CustomLibrary.randomPhone(context);
		CustomLibrary.randomEmail(context);
		CustomLibrary.randomName(context);
		CustomLibrary.randomAddress(context);
		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String orderItemId = (String) context.getAttribute("orderItemId");
		String state = (String) context.getAttribute("state");
		String city = (String) context.getAttribute("city");
		String zipcode = (String) context.getAttribute("zipcode");
		String phone = (String) context.getAttribute("phone");
		String email = (String) context.getAttribute("email");
		String firstName = (String) context.getAttribute("firstName");
		String lastName = (String) context.getAttribute("lastName");
		String address1 = (String) context.getAttribute("address1");
		String putBody = "{" + "\"recipient\": {" 
				+ "\"address1\": \"" + address1 + "\","
				+ "\"address2\": \"\"," 
				+ "\"address3\": \"\","
				+ "\"addressId\": \"\"," 
				+ "\"city\": \"" + city+ "\"," 
				+ "\"confirmEmail\": \"" + email + "\"," 
				+ "\"country\": \"USA\"," 
				+ "\"email\": \"" + email+ "\"," 
				+ "\"firstName\": \"" + firstName + "\"," 
				+ "\"isAddressVerified\": \"Y\","
				+ "\"keepThisAddress\": \"true\"," 
				+ "\"lastName\": \"" + lastName + "\","
				+ "\"locationType\": \"Residence\"," 
				+ "\"organizationName\": \"QA AUTO TEST COMP\","
				+ "\"phone\": \"" + phone + "\","
				+ "\"state\": \"" + state + "\"," 
				+ "\"zipCode\": \"" + zipcode
				+ "\"" + "}" + "}";
		System.out.println("Test Data(" + testCase + "," + orderItemId + "," + brand + "," + putBody + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(putBody);
		Response response = httpRequest.request(Method.PUT,
				"/r/api/checkout/cart/" + orderItemId + "/recipient?brand=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderItemId:" + orderItemId);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
