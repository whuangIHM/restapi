package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class PostCartOrderIdCheckout {
	// https://uat1.origin-gcp.800-flowers.net/checkout/cart/123/chekcout?brand=flowers
	@Test
	public static void postCartOrderIdCheckout(ITestContext context) {
		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String orderId = (String) context.getAttribute("orderId");
		String postBody = "{ \"billingAddress\": " 
				+ "{ \"address1\": \"1 old country rd\", "
				+ "\"address2\": \"ste500\", " 
				+ "\"address3\": \"\", " 
				+ "\"addressId\": \"\", "
				+ "\"city\": \"carle place\", " 
				+ "\"confirmEmail\": \"sreshi@gmail.com\", " 
				+ "\"country\": \"US\", "
				+ "\"email\": \"sreshi@gmail.com\", " 
				+ "\"firstName\": \"Sajad\"," 
				+ "\"lastName\": \"Sajad\", "
				+ "\"locationType\": \"Residence\", " 
				+ "\"organizationName\": \"\", " 
				+ "\"phone\": \"1234567890\", "
				+ "\"state\": \"NY\", " 
				+ "\"zipCode\": \"11514\" }, "
				+ "\"order\": { \"orderTotal\": \"102.96\" }, "
				+ "\"payment\": { \"accountNumber\": \"5454545454545454\", " 
				+ "\"amount\": \"76.96\", "
				+ "\"cvv\": \"123\", " 
				+ "\"expirationMonth\": \"6\", " 
				+ "\"expirationYear\": \"2019\", "
				+ "\"nameOnCreditCard\": \"Sajad Reshi\", " 
				+ "\"paymentMethod\": \"creditcard\", "
				+ "\"savePaymentMethod\": true } " + "}";
		System.out.println("=======================================================================");
		System.out.println("PostCartOrderIdCheckout================================================");
		System.out.println("=======================================================================");
		System.out.println("Test Data(" + testCase + "," + orderId + "," + brand + "," + postBody + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(postBody);
		Response response = httpRequest.request(Method.POST,
				"/r/api/checkout/cart/" + orderId + "/checkout?brand=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderId: " + orderId);// +" Token: " +tokenSession);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
