package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class PostZipcodeValidate {
	// https://uat1.origin-gcp.800-flowers.net/checkout/zipcode/11367/validate?brand=flowers
	@Test
	public static void postZipcodeValidate(ITestContext context) {
		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String zipcode = (String) context.getAttribute("zipcode");
		String orderId = (String) context.getAttribute("orderId");
		System.out.println("=======================================================================");
		System.out.println("PostZipcodeValidate====================================================");
		System.out.println("=======================================================================");
		System.out.println("Test_Data(" + testCase + "," + orderId + "," + brand + "," + zipcode + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		Response response = httpRequest.request(Method.POST,
				"/r/api/checkout/zipcode/" + zipcode + "/validate?brand=flowers=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderId: " + orderId);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
