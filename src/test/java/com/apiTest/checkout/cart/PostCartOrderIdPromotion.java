package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class PostCartOrderIdPromotion {
	// https://uat1.origin-gcp.800-flowers.net/checkout/cart/123/promotion?brand=flowers
	@Test
	public static void postCartOrderIdPromotion(ITestContext context) {
		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String orderId = (String) context.getAttribute("orderId");
		String postBody = "{"
				+ "\"currentPayment\": \"\", " 
				+ "\"existingPromotionCode\": \"\", "
				+ "\"membershipId\": \"9055\", " + "\"promotionCode\": \"12M\", " + "\"promotionZipCode\": \"11365\" "
				+ "}";
		System.out.println("=======================================================================");
		System.out.println("PostCartOrderIdPromotion===============================================");
		System.out.println("=======================================================================");
		System.out.println("Test Data(" + testCase + "," + orderId + "," + brand + "," + postBody + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(postBody.toString());
		Response response = httpRequest.request(Method.POST,
				"/r/api/checkout/cart/" + orderId + "/promotion?brand=flowers=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		// Debug only
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderId: " + orderId);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
