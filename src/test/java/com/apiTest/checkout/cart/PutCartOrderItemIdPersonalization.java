package com.apiTest.checkout.cart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.apiTest.lib.CustomLibrary;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public class PutCartOrderItemIdPersonalization {
	// https://uat1.origin-gcp.800-flowers.net/checkout/cart/123/personalizationResponse?brand=flowers
	@Test
	public static void putCartOrderItemIdPersonalization(ITestContext context) {
		String testCase = (String) context.getAttribute("testCase");
		String brand = (String) context.getAttribute("brand");
		String orderItemId = (String) context.getAttribute("orderItemId");
		String brandCode = (String) context.getAttribute("brandCode");
		String productCode = (String) context.getAttribute("productCode");
		String putBody = "{" + 
						"\"brandCode\": \""+brandCode+"\"," + 
						"\"itemType\": \"\"," + 
						"\"productCode\": \""+productCode+"\"," + 
						"\"response\": {" + 
						"\"responseData\": \"\"" + 
						"}";
		System.out.println("=======================================================================");
		System.out.println("PutCartOrderItemIdPersonalization========================================");
		System.out.println("=======================================================================");
		System.out.println("Test Data(" + testCase + "," + orderItemId + "," + brand + "," + putBody + ")");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(putBody);
		Response response = httpRequest.request(Method.PUT,
				"/r/api/checkout/cart/" + orderItemId + "/personalizationResponse?brand=" + brand);
		CustomLibrary.softAssert.assertEquals(response.getStatusCode(), 200, "Status Code Failed!");
		String responseBody = response.getBody().asString();
		if (responseBody.isEmpty()) {
			CustomLibrary.softAssert.assertTrue(false, "No Response Body is empty******************************");
		} else {
			context.setAttribute("response", responseBody);
			System.out.println("OrderItemId:" + orderItemId);
			System.out.println("Response: " + responseBody);
		}
		CustomLibrary.softAssert.assertAll();
	}
}
