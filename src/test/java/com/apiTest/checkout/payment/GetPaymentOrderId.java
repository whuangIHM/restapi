package com.apiTest.checkout.payment;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public interface GetPaymentOrderId {
//	https://uat1.origin-gcp.800-flowers.net/checkout/payment/213?brand=flowers
	@Test
	public static void getPaymentOrderId(ITestContext context) {
		String orderId = (String) context.getAttribute("orderId");
		String brand = (String) context.getAttribute("brand");
		// baseUri on Custom Listener
		RestAssured.baseURI = (String) context.getAttribute("baseUri");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		Response response = httpRequest.request(Method.GET, "/r/api/checkout/payment/" + orderId + "?brand=" + brand);
		String responseBody = response.getBody().asString();
		context.setAttribute("response", responseBody);
	}
}
