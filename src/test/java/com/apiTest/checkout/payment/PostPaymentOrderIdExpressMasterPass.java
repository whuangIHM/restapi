package com.apiTest.checkout.payment;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Test NG
 */
public interface PostPaymentOrderIdExpressMasterPass {
//	String postBody = "{" + 
//						"\"checkout_resource_url\": \"string\"," + 
//						"\"oauth_token\": \"string\"," + 
//						"\"oauth_verifier\": \"string\"" + 
//						"}";
	// https://uat1.origin-gcp.800-flowers.net/checkout/payment/123/express/masterPass?brand=flowers
	@Test
	public static void postPaymentOrderIdExpressMasterPass(ITestContext context) {
		String orderId = (String) context.getAttribute("orderId");
		String brand = (String) context.getAttribute("brand");
		String postBody = (String) context.getAttribute("postBody");
		RestAssured.baseURI = (String) context.getAttribute("baseUri");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(postBody);
		Response response = httpRequest.request(Method.POST,
				"/r/api/checkout/payment/" + orderId + "/express/masterPass?brand=" + brand);
		String responseBody = response.getBody().asString();
		context.setAttribute("response", responseBody);
	}
}
