package com.apiTest.checkout.payment;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public interface DeletePaymentOrderIdGiftCard {
	// https://uat1.origin-gcp.800-flowers.net/checkout/payment/123/giftcard?brand=test
	@Test
	public static void deletePaymentOrderIdGiftCard(ITestContext context) {
//		String postBody = "{" + 
//				"\"account\": \"string\"," + 
//				"\"giftCardId\": \"string\"" + 
//				"}";
		String orderId = (String) context.getAttribute("orderId");
		String brand = (String) context.getAttribute("brand");
		String postBody = (String) context.getAttribute("postBody");
		RestAssured.baseURI = (String) context.getAttribute("baseUri");
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("content-type", "application/json");
		httpRequest.header("Authorization", context.getAttribute("accessToken"));
		httpRequest.body(postBody);
		Response response = httpRequest.request(Method.DELETE,
				"/r/api/checkout/payment/" + orderId + "/giftcard?brand=" + brand);
		String responseBody = response.getBody().asString();
		context.setAttribute("response", responseBody);
	}
}
