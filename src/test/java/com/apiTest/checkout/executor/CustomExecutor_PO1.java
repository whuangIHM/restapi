package com.apiTest.checkout.executor;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Factory;
//import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.apiTest.checkout.cart.*;
import com.apiTest.csv.CSVDataProvider;
import com.apiTest.csv.DataProviderClass;

import io.restassured.RestAssured;

//@Listeners(com.apiTest.lib.CustomListener.class)	
public class CustomExecutor_PO1 {
	String headers = "TestCase,OrderId,Response,TestStatus";
	String testCase = "TC_1", testStatus = "true";
	String brand = "flowers", orderId = "123";
	String orderItemId = "123", promotionCode = "12M";
	String startDate = "10/10/2018", deliveryDate = "10/10/2018";
	String estimateShipping = "true";
	String brandCode = "1012", productCode = "11111", quantity ="1";
	String email,phone,address1,firstName,lastName,city,state,zipcode;
	String response;
	String baseUri = "https://uat1.origin-gcp.800-flowers.net";
	String guid = "m9eKwXTkeJUL7crsjNlJEd32ZOvgGLlP MCnWnvQHx3Om3nCNxd0bthE8KQv9HSst-6CW0erd_Zgq0HcGNY78IQuKrpTdRJ71";

	static final boolean postCartEnabled = true;

	@Factory(dataProvider = "postCartData", dataProviderClass = DataProviderClass.class)
	public CustomExecutor_PO1(String testCase, String brand, 
							String brandCode, String productCode, 
							String quantity, String estimateShipping,
							String city, String state, String zipcode) {
		this.testCase = testCase;
		this.brand = brand;
		this.brandCode = brandCode;
		this.productCode = productCode;
		this.quantity = quantity;
		this.estimateShipping = estimateShipping;
		this.city = city;
		this.state = state;
		this.zipcode = zipcode;
	}
	@BeforeTest
	public void beforeTest(ITestContext context) {}

	@BeforeClass
	public void beforeClass(ITestContext context) {}

	// ASSIGN VALUE
	@BeforeMethod
	public void beforeMethod(ITestContext context) {
		System.out.println("========================Start==========================================");

		context.setAttribute("testCase", testCase);
		context.setAttribute("brand", brand);
		context.setAttribute("brandCode", brandCode);
		context.setAttribute("productCode", productCode);
		context.setAttribute("quantity", quantity);
		context.setAttribute("estimateShipping", estimateShipping);
		context.setAttribute("city", city);
		context.setAttribute("state", state);
		context.setAttribute("zipcode", zipcode);

		// BaseUri and GUID
		context.setAttribute("baseUri", baseUri);
		context.setAttribute("guid", guid);
		RestAssured.baseURI = (String) context.getAttribute("baseUri");
		// Get Guest Token
		System.out.println("=======================================================================");
		System.out.println("GetGuestToken Init ====================================================");
		System.out.println("=======================================================================");
		GetGuestToken.getGuestToken(context);
		System.out.println("Access Token is =>  " + context.getAttribute("accessToken"));
		System.out.println("=======================================================================");

	}

	@Test( priority = 1, groups = "testpostCart", enabled = postCartEnabled)
	public void testpostCart(ITestContext context) {
		PostCart.postCart(context);
		GetCartCount.getCartCount(context);
		// NOT WORKING GET STATUS CODE 401 NO AUTH
//        GetCartHolidayOccasionList.getCartHolidayOccasionList(context);
//		for (int i = 1; i <= 1; i++) {
		PutCartOrderItemIdRecipient.putCartOrderItemIdRecipient(context);
//		}
		GetCart.getCart(context);
		GetCartOrderItemIdDeliveryCalendar.getOrderItemIdDeliveryCalendar(context);
		GetCart.getCart(context);
		GetCartOrderItemIdDTW.getOrderItemIdDTW(context);
		GetCart.getCart(context);
		PutCartOrderIdCalculateshipping.putCartOrderIdCalculateshipping(context);
		GetCart.getCart(context);
		// NOT WORKING GET STATUS CODE 404
//     	PostCartOrderIdPromotion.postCartOrderIdPromotion(context);
//		GetCart.getCart(context);
		PostCartOrderIdCheckout.postCartOrderIdCheckout(context);
		GetCart.getCart(context);
	}
//	@Test( priority = 2, dependsOnGroups = "testpostCart", enabled = getCartCountEnabled)
//	public void testgetCartCount(ITestContext context) {
//		GetCartCount.getCartCount(context);
//	}

	//add
	@AfterMethod(alwaysRun = true)
	public void afterMethod(ITestContext context, ITestResult result) {
		testCase = (String) context.getAttribute("testCase");
		response = (String) context.getAttribute("response");
		orderId = (String) context.getAttribute("orderId");
		orderItemId = (String) context.getAttribute("orderItemId");
		
		if (result.isSuccess() == true && context.getAttribute("response").toString().contains("\"status\":\"M\"")) {
			testStatus = "Pass";
			System.out.println("=======================================================================");
			CSVDataProvider.setData(headers,result.getName() + "_FinalResult.csv", testCase, orderId, response, testStatus);
		} else {
			testStatus = "Fail";
			System.out.println("=======================================================================");
			CSVDataProvider.setData(headers,result.getName() + "_FinalResult.csv", testCase, orderId, response, testStatus);
		}	
		System.out.println(result.getName() + "-" + testCase + ": " + testStatus);
		System.out.println("=========================END===========================================");
	}
}
